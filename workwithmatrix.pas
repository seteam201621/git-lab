unit WorkWithMatrix;

interface
const
   size = 7;
type 
   Matrix=array [1..size, 1..size] of real;
   
procedure CreateMatrix(var _matrix:Matrix);
procedure PrintMatrix(_matrix:Matrix);
function QuantityNegativeElementsOnTheMainDiagonal(_matrix:Matrix):integer;
function IfElementAboveMainDiagonal(i,j:integer):boolean;
procedure ReplasePositiveElementsAboveMainDiagonaleToNull(var _matrix:Matrix);

implementation

procedure CreateMatrix(var _matrix:Matrix);
   var i, j: integer;
   const MaxElement = 12;
         MinElement = -14;
begin
    randomize;
    for i := 1 to size do
        for j := 1 to size do
            _matrix[i,j] := random*(MaxElement - MinElement) + MinElement;
end;

procedure PrintMatrix(_matrix:Matrix);
   var i, j: integer;
begin
   for i := 1 to size do
      begin
        for j := 1 to size do
            write(_matrix[i,j]:6:2,' ');
      writeln;      
      end;
   writeln;
end;

function QuantityNegativeElementsOnTheMainDiagonal(_matrix:Matrix):integer;
var i, quantityElements: integer;

begin
    quantityElements := 0;
    for i := 1 to size do
    begin
        if _matrix[i,i] < 0 then
        begin
            quantityElements := quantityElements + 1;
        end;
    end;
    QuantityNegativeElementsOnTheMainDiagonal:=quantityElements;
end; 

function IfElementAboveMainDiagonal(i,j:integer):boolean;
begin
    if (i<j) then IfElementAboveMainDiagonal:=true
    else IfElementAboveMainDiagonal:=false;
end;

procedure ReplasePositiveElementsAboveMainDiagonaleToNull(var _matrix:Matrix);
   var i,j:integer;
begin
   for i := 1 to size do
        for j := 1 to size do
           if (_matrix[i,j]>0) and IfElementAboveMainDiagonal(i,j) then  _matrix[i,j] := 0;
end;
end.



