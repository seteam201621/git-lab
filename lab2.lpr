Program LaboratoryWork2;
uses WorkWithMatrix;
var givenMatrix:Matrix;
begin
  CreateMatrix(givenMatrix);
  PrintMatrix(givenMatrix);
  writeln('Number of negative elements on the main diagonal = ',QuantityNegativeElementsOnTheMainDiagonal(givenMatrix));
  writeln;
  ReplasePositiveElementsAboveMainDiagonaleToNull(givenMatrix);
  PrintMatrix(givenMatrix);
  readln;
end.
